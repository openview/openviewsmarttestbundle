OpenviewSmarTestBundle
======================

This bundle contains "smart" classes to make writing tests for Symfony2 faster and easier.

It provides an architecture where all the fixtures loaded are tracked, almost automatically, and removed at the end of the tests, the developer does not need to write any additional line of code. 

Within the test classes of the bundle there are also useful methods to use to speedup the coding.

The bundle is also compatible with the the [Behat/Mink](http://mink.behat.org/) test suite.
_More installation info coming soon_

## Installation
_Provide info on how to add a custom composer repo and install the bundle_

## TODO

* Improve the SmartFixture class
* Set up some parametric configuration for those values that are not static, such as the AppKernel class location
* Improve the Mink support
* _Test the Tests_

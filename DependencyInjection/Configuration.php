<?php

namespace Openview\SmartTestBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('openview_smart_test');

        // See also http://php-and-symfony.matthiasnoback.nl/2012/05/symfony2-config-component-config-definition-and-processing/

        $rootNode
            ->children()
                ->arrayNode('mink')
                    ->children()
                        ->scalarNode('base_url')->end()
                        ->scalarNode('browser_name')->defaultValue('chrome')->end()
                    ->end()
                ->end()
            ->end()
        ;

        return $treeBuilder;
    }
}

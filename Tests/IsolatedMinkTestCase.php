<?php 

namespace Openview\SmartTestBundle\Tests;

// See Mink usage example at https://github.com/Behat/Mink/blob/master/README.md
// It's there at 2012-07-04 14:24 GTM:Rome, but at Behat they like to change stuff
// without notice so it's not really a reliable source...
use Behat\Mink\Mink,
    Behat\Mink\Session,
    Behat\Mink\Driver\GoutteDriver,
    Behat\Mink\Driver\Goutte\Client as GoutteClient,
    Behat\Mink\Driver\SahiDriver;
use Openview\IlmioambulatorioBundle\Tests\DataFixtures\UserData;
use Openview\SmartTestBundle\Tests\IsolationHelper;
use Openview\SmartTestBundle\Tests\IsolatedTestInterface;
use Openview\SmartTestBundle\Tests\SessionAwareTestInterface;

class IsolatedMinkTestCase extends \PHPUnit_Framework_TestCase implements IsolatedTestInterface, SessionAwareTestInterface
{
    private $isolationHelper;
    
    // Protected props that have to be used by subclasses
    protected $session;
    protected $base; 
    protected $mink;

    public function __construct()
    {
        parent::__construct();
        // Init the isolation helper
        $this->isolationHelper = new IsolationHelper();
    }
    
    public function loadDocumentFixtureData($fixture)
    {
        $this->isolationHelper->loadDocumentFixtureData($fixture);
    }
    
    public function loadEntityFixtureData($fixture)
    {
        $this->isolationHelper->loadEntityFixtureData($fixture);
    }
    
    public function registerCreatedDocument($document)
    {
        $this->isolationHelper->registerCreatedDocument($document);
    }   
    
    public function registerCreatedEntity($entity)
    {
        $this->isolationHelper->registerCreatedEntity($entity);
    }   
    
    public function getAppContainer()
    {
        return $this->isolationHelper->getAppContainer();
    }
    
    public function getDocumentManager()
    {
        return $this->isolationHelper->getDocumentManager();
    }
    
    public function getEntityManager()
    {  
        return $this->isolationHelper->getEntityManager();
    }
    
    public function getUserManager()
    {
        return $this->isolationHelper->getUserManager();
    }

    public function login($context, $username = 'Pippo', $password = 'password') {
        
        $session = $context;
        
        $session->visit($this->base . '/login');
        
        $page = $session->getPage();

        $username = $page->findField('_username');
        $password = $page->findField('password');
        $loginBtn = $page->findButton('_submit');
        
        $username->setValue('Pippo');
        $password->setValue('password');
        
        $loginBtn->press();
        
        // Remember the session so we can logout;
        $this->session = $context;
    }
  
    // Load the Fixtures
    public function setUp()
    {
        parent::setUp();

        $this->base = 'http://ilmioambulatorio.dev/app_dev.php'; // TODO REMOVE ASAP

        $this->mink = new Mink(array(
            // It's bugged
            //'goutte'    => new Session(new GoutteDriver(new GoutteClient($this->base))),
            'sahi' => new Session(new SahiDriver('chrome')),
        ));

        $this->mink->setDefaultSessionName('sahi');
            
        // We will always need the users, so we load them here
        $this->loadEntityFixtureData(new UserData($this->getAppContainer()));
    }
    
    // Clean up
    public function tearDown()
    {   
        if($this->session) {
            $this->session->visit($this->base . '/logout');
        }
        
        $this->isolationHelper->tearDown();
        
        parent::tearDown();
    }
}

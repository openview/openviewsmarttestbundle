<?php
/*
 *  See http://blog.sznapka.pl/fully-isolated-tests-in-symfony2/
 */

namespace Openview\SmartTestBundle\Tests;

// This class should be in /vendor/openview/smart-test/Openview/SmartTestBundle/Tests/
// so we have to go up 6 levels
require_once(__DIR__ . "/../../../../../../app/AppKernel.php");

use Doctrine\Common\DataFixtures\Loader;
use Doctrine\Common\DataFixtures\Executor\MongoDBExecutor;
use Doctrine\Common\DataFixtures\Purger\MongoDBPurger;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;

class IsolationHelper
{
    private $_container;
    private $createdEntities;
    private $createdDocuments;
    private $documentManager;
    private $entityManager;
    private $userManager;
    
    public function __construct()
    {
        $kernel = new \AppKernel("test", true);
        $kernel->boot();
        $this->_container = $kernel->getContainer();        
        $this->documentManager = $this->getServiceIfAny( 'doctrine.odm.mongodb.document_manager' );
        $this->entityManager = $this->getServiceIfAny( 'doctrine.orm.entity_manager' );
        $this->userManager = $this->getServiceIfAny( 'fos_user.user_manager' );
        $this->createdEntities = array();        
        $this->createdDocuments = array();
    }

    private function getServiceIfAny($service)
    {
        if($this->_container->has($service)) {
            return $this->_container->get($service);
        }
        return null;
    }
    
    private function getService($service)
    {
        return $this->_container->get($service);
    }
    
    public function getAppContainer()
    {
        return $this->_container;
    }
    
    public function getDocumentManager()
    {
        return $this->documentManager;
    }
    
    public function getEntityManager()
    {  
        return $this->entityManager;
    }
    
    public function getUserManager()
    {
        return $this->userManager;
    }
    
    public function loadDocumentFixtureData($fixture)
    {
        $loader = new Loader;
        $loader->addFixture($fixture);
        $purger = new MongoDBPurger($this->documentManager);
        $executor = new MongoDBExecutor($this->documentManager, $purger);
        $executor->execute($loader->getFixtures(), true);
        
        /*
         * There should be no problem with duplicated or already present objects because:
         * "If the input arrays have the same string keys, then the later value for that key will overwrite the previous one. 
         * If, however, the arrays contain numeric keys, the later value will not overwrite the original value, but will be appended."
         * see: http://www.php.net/manual/en/function.array-merge.php
         */
        $this->createdDocuments = array_merge($this->createdDocuments, $fixture->getCreatedDocuments());
    }
    
    public function loadEntityFixtureData($fixture)
    {
        $loader = new Loader;
        $loader->addFixture($fixture);
        $purger = new ORMPurger($this->entityManager);
        $executor = new ORMExecutor($this->entityManager, $purger);
        $executor->execute($loader->getFixtures(), true);
        $this->createdEntities = array_merge($this->createdEntities, $fixture->getCreatedEntities());         
    }
    
    public function registerCreatedDocument($document)
    {
        $this->createdDocuments[] = $document;
    }
    
    public function registerCreatedEntity($entity)
    {
        $this->createdEntities[] = $entity;
    }
    
    public function tearDown()
    {
        foreach($this->createdEntities as $ce)
        {
            // Fix to avoid detached entity problems.
            // Try to comment it and remove $ce instead to see what I mean
            // More info about the problem here: http://docs.doctrine-project.org/projects/doctrine-orm/en/2.0.x/reference/working-with-objects.html#detaching-entities
            // I think is due to the fact that components like FOSUserBundle userManager use a different
            // instance of the entity manager, so $this->entityManager does not know
            // about the entities -Gio'
            $managedEnetity = $this->entityManager->merge($ce);
            $this->entityManager->remove($managedEnetity);
        }
        $this->entityManager->flush();

        foreach($this->createdDocuments as $cd)
        {
            $managedDocument = $this->documentManager->merge($cd);
            $this->documentManager->remove($managedDocument);
        }
        $this->documentManager->flush();
    }
}

<?php 

namespace Openview\SmartTestBundle\Tests;

interface SmartTestInterface
{
    /**
	 *	Test the response to check if the request was succesful
	 */
    public function assertIsSuccessful($response);
}

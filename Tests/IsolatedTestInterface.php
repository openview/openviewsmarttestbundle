<?php 

namespace Openview\SmartTestBundle\Tests;

/*
 *  These are the methods contained in the IsolationHelper
 */
interface IsolatedTestInterface
{
    public function loadDocumentFixtureData($fixture);
    
    public function loadEntityFixtureData($fixture);
    
    public function registerCreatedDocument($document);
    
    public function registerCreatedEntity($entity);
    
    public function getDocumentManager();
    
    public function getEntityManager();
    
    public function getUserManager();
    
    public function getAppContainer();
}

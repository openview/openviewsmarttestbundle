<?php 

namespace Openview\SmartTestBundle\Tests;
 
use Openview\SmartTestBundle\Tests\IsolationHelper;
use Openview\SmartTestBundle\Tests\IsolatedTestInterface;
use Openview\SmartTestBundle\Tests\SessionAwareTestInterface;
use Openview\SmartTestBundle\Tests\SmartTestInterface;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class IsolatedWebTestCase extends WebTestCase implements IsolatedTestInterface, SessionAwareTestInterface, SmartTestInterface
{
    private $isolationHelper;
    
    public function __construct()
    {
        parent::__construct();
        $this->isolationHelper = new IsolationHelper();
    }
    
    public function loadDocumentFixtureData($fixture)
    {
        $this->isolationHelper->loadDocumentFixtureData($fixture);
    }
    
    public function loadEntityFixtureData($fixture)
    {
        $this->isolationHelper->loadEntityFixtureData($fixture);
    }
    
    public function registerCreatedDocument($document)
    {
        $this->isolationHelper->registerCreatedDocument($document);
    }   
    
    public function registerCreatedEntity($entity)
    {
        $this->isolationHelper->registerCreatedEntity($entity);
    }   
    
    public function getAppContainer()
    {
        return $this->isolationHelper->getAppContainer();
    }
    
    public function getDocumentManager()
    {
        return $this->isolationHelper->getDocumentManager();
    }
    
    public function getEntityManager()
    {  
        return $this->isolationHelper->getEntityManager();
    }
    
    public function getUserManager()
    {
        return $this->isolationHelper->getUserManager();
    }
    
    public function login($context, $username = 'Pippo', $password = 'password') {
        
        $client = $context;
        
        $crawler = $client->request('GET', '/login');
        $form = $crawler->selectButton('Login')->form();
        $client->submit($form, array('_username' => $username, '_password' => $password));
    
    }
  
    // Load the Fixtures
    public function setUp()
    {
        parent::setUp();
    }
    
    // Clean up
    public function tearDown()
    {        
        // TODO remove once migration is completed
        foreach($this->createdDocuments as $doc) {
            $this->isolationHelper->registerCreatedDocument($doc);
        }
        foreach($this->createdEntities as $entity) {         
            $this->isolationHelper->registerCreatedEntity($entity);
        }
        
        $this->isolationHelper->tearDown();
        parent::tearDown();
    }

    /**
     *  @param Symfony\Component\HttpFoundation\Response $response The response to test
     *
     *  @return boolean true if the response has a 200 HTTP code
     */
    public function assertIsSuccessful($response)
    {
        return $this->assertEquals(200, $response->getStatusCode(),
            'Request was not succesful');
    }
}

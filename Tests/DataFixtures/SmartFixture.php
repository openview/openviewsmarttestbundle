<?php 

namespace Openview\SmartTestBundle\Tests\DataFixtures;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class SmartFixture implements FixtureInterface
{
    protected $createdEntities;
    protected $createdDocuments;
    
    public function __construct()
    {
        $this->createdEntities = array();
        $this->createdDocuments = array();
    }
    
    public function getCreatedDocuments()
    {
        return $this->createdDocuments;
    }
    
    public function getCreatedEntities()
    {
        return $this->createdEntities;
    }
    
    public function registerDocument($document)
    {
        if($document == null) { return false; }
            
        $this->createdDocuments[] = $document;
    }
    
    public function registerEntity($entity)
    {
//echo("registering entity ");
        if($entity == null) { return false; }        
        $this->createdEntities[] = $entity;
//echo("size" . count($this->createdEntities) . ' ');
    }
    
    // Here just to be compliant with the interface    
    public function load(ObjectManager $manager)
    {
    }
}

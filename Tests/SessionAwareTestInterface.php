<?php 

namespace Openview\SmartTestBundle\Tests;

interface SessionAwareTestInterface
{
    public function login($context, $username, $password);
}
